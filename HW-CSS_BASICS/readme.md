#### Deliverables Format

- File should be a _.zip_ and named following next pattern: `WU-013__HW-{NAME_OF_HW}__{SURNAME_NAME}`
  - example: `WU-013__HW-HTML_TAGS__BOND_JAMES`
- Source files should be properly formatted and validated

----

#### Task

Take a look in _designs_ folder and find __2__ themes for __2__ pages - __culture.html__ and __history.html__.

- Create __2__  CSS files so that your pages look as similar to designs as possible.
  - CSS files should be named __styles/avocado.css__ and __styles/peach.css__ correspondingly
  - Use __styles/example.css__ as a starting point
  - Try to use only given _CSS_ selectors in _styles/example.css_
  - Try not to overbloat your style with unnecessary properties