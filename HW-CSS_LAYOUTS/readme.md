#### Deliverables Format

- File should be a _.zip_ and named following next pattern: `WU-013__HW-{NAME_OF_HW}__{SURNAME_NAME}`
  - example: `WU-013__HW-HTML_TAGS__BOND_JAMES`
- Zip unarchiving should result in a folder with the same name as described above.
- Source files should be properly formatted and validated

----

#### Task

- Build __2__ webpages that are similar in layout to __mockup.png__.
- 2 layouts should be based on _float_ and _flex-box_ properties/approaches.
- HTML for different approacehs may differ and thats why there are separate _index.html_ per approach.
- Use some basic styles to make mockup look a bit _nicer_ than the mockup picture itself.

