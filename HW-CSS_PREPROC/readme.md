#### Deliverables Format

- File should be a _.zip_ and named following next pattern: `WU-013__HW-{NAME_OF_HW}__{SURNAME_NAME}`
  - example: `WU-013__HW-HTML_TAGS__BOND_JAMES`
- Zip unarchiving should result in a folder with the same name as described above.
- Source files should be properly formatted and validated

----

#### Task

- Download a tool of choice to compile `scss` to `css`:
  - GUI app such as http://koala-app.com/
  - Or http://sass-lang.com/install
  - Or https://github.com/sass/node-sass
- Write styles in `_main.scss`, `_variables_peach.scss` and `_variables_avocado.scss`
- In result there should be __2 css__ files in `css` folder for _avocado_ and _peach_ designs
