#### Deliverables Format

- File should be a _.zip_ and named following next pattern: `WU-013__HW-{NAME_OF_HW}__{SURNAME_NAME}`
  - example: `WU-013__HW-HTML_TAGS__BOND_JAMES`
- Source files should be properly formatted and validated

----

#### Task

- Use __index.html__ to build an order form as similar to the _example.png_ picture as possible.
- Only _tags_ and _attributes_ are __permitted__ to be used in this task
- All input fields should use a proper type (e.g. text, email, date)
- One field should be disabled
- One field should have some value prepopulated and be readonly

Note: Form elements may differ visually depending on OS