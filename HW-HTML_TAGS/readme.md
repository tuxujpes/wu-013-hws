#### Deliverables Format

- File should be a _.zip_ and named following next pattern: `WU-013__HW-{NAME_OF_HW}__{SURNAME_NAME}`
  - example: `WU-013__HW-HTML_TAGS__BOND_JAMES`
- Source files should be properly formatted and validated

----

#### Task

- Use __index.html__ to build page as similar to the _example.png_ picture as possible using text from below.
- Do __not__ use any css in this task!

```
HTML Elements

Below is just about everything you’ll need to style in the theme. Check the source code to see the many embedded elements within paragraphs.

Heading 1
Heading 2
Heading 3
Heading 4
Heading 5
Heading 6

Lorem ipsum dolor sit amet, test link adipiscing elit. This is strong. Nullam dignissim convallis est. Quisque aliquam. This is emphasized. Donec faucibus. Nunc iaculis suscipit dui. 53 = 125. Water is H2O. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. The New York Times (That’s a citation). Underline. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.

HTML and CSS are our tools. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. To copy a file type COPY filename.  Let’s make that 7. This text has been struck.

List Types

Definition List

Definition List Title
This is a definition list division.

Definition
An exact statement or description of the nature, scope, or meaning of something: our definition of what constitutes poetry.

Ordered List
List Item 1
List Item 2
Nested list item A
Nested list item B
List Item 3

Unordered List
List Item 1
List Item 2
Nested list item A
Nested list item B
List Item 3

Preformatted Text

Typographically, preformatted text is not the same thing as code. Sometimes, a faithful execution of the text requires preformatted text that may not have anything to do with code. Most browsers use Courier and that’s a good default — with one slight adjustment, Courier 10 Pitch over regular Courier for Linux users. For example:

“Beware the Jabberwock, my son!
    The jaws that bite, the claws that catch!
Beware the Jubjub bird, and shun
    The frumious Bandersnatch!”

Code

Code can be presented inline, like <?php bloginfo('stylesheet_url'); ?>, or within a<pre> block. Because we have more specific typographic needs for code, we’ll specify Consolas and Monaco ahead of the browser-defined monospace font.

#container {
	float: left;
	margin: 0 -240px 0 0;
	width: 100%;
}

Blockquotes

Let’s keep it simple. Italics are good to help set it off from the body text. Be sure to style the citation.
Good afternoon, gentlemen. I am a HAL 9000 computer. I became operational at the H.A.L. plant in Urbana, Illinois on the 12th of January 1992. My instructor was Mr. Langley, and he taught me to sing a song. If you’d like to hear it I can sing it for you.

— HAL 9000
```