#### Deliverables Format

- File should be a _.zip_ and named following next pattern: `WU-013__HW-{NAME_OF_HW}__{SURNAME_NAME}`;
  - example: `WU-013__HW-HTML_TAGS__BOND_JAMES`;
- Zip unarchiving should result in a folder with the same name as described above;
- Source files should be working, properly formatted and validated;
- __IMPORTANT!__ Do not remove code at the end of `script.js` file:
  ```
  module.exports = {
    isBigger: isBigger,
    isSmaller: isSmaller,
    getMin: getMin,
    getClosestToZero: getClosestToZero,
    isPrime: isPrime
  }
  ```

----

#### Task

- __isBigger__ - a function which accepts two arguments and returns true if first one has greater value than second one or false otherwise.  
_E.g._: `isBigger(5, -1); // => true`  
_Note_: Do not use if/else clauses nor ternary operators in this task  

- __isSmaller__ - a function which accepts two arguments and returns true if first one has lesser value than second one or false otherwise.  
_E.g._ `isSmaller(5, -1); // => false`  
_Note_: Reuse __isBigger__ function  

- __getMin__ - a function which accepts arbitrary number of integer arguments and returns the one with the smallest value.  
_E.g._ `getMin(3, 0, -3); // => -3`  
_Note_: since arguments is like _array_, you can use simple _for_ iteration over it  
and use `arguments[ i ]` to get the argument of a given index  

- __getClosestToZero__ - a function which accepts arbitrary number of integer arguments and returns one closest to 0 (zero).  
In case when there are integers that are similarly close to zero, pick the one that is positive!  
_E.g._ `getClosestToZero(9, 5, -4, -9); // => -4`  
_E.g._ `getClosestToZero(5, -2, 2, -5); // => 2`  
_Note_: `Math.abs()` might be helpful  

- __isPrime__ - a function which accepts one integer argument and returns true if it is prime number or false otherwise (http://www.mathsisfun.com/definitions/prime-number.html).  
_E.g._ `isPrime(5); // => true`  

- __guessANumber__ - rewrite guess a number game from previous homework using functions.  
The main idea is to reduce keep your code [DRY](https://en.wikipedia.org/wiki/Don't_repeat_yourself) by reusing same bits of code.  
Making your game much more readable and maintainable.  
Note: game should be started by calling `guessANumber()` function

