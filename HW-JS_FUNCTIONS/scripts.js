/* WRITE YOUR CODE HERE */
function isBigger(a,b) {
  return a > b;
}

function isSmaller(b, a) {
  return isBigger(a, b);
}

function getMin() {
  var min = Infinity;
  for (var i = 0; i < arguments.length; i++){
    min = Math.min(arguments[i], min);
  }
  return min;
}

function getClosestToZero() {
  var min = Infinity;
  for (var i = 0; i < arguments.length; i++){
    if(Math.abs(arguments[i]) < Math.abs(min)) {
      min = arguments[i];
    } else if (Math.abs(arguments[i]) === Math.abs(min) && (arguments[i] > 0)) {
      min = arguments[i];
    }
  }
  return min;
}


function isPrime(num){
  for(var i = 2; i < num; i++){
    if ((num % i) === 0) { 
      return false;
    }
  }
  return num > 1;
}

/* DO NOT REMOVE EXPORTS BELOW */
module.exports = {
  isBigger: isBigger,
  isSmaller: isSmaller,
  getMin: getMin,
  getClosestToZero: getClosestToZero,
  isPrime: isPrime
}