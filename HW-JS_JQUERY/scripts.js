(function($) {

  $.fn.accordion = function() {

    $(this).addClass('accordion');
    $(this).next().addClass('panel');
    $('.panel').hide();
    $(this).on('click', function() {
      $(this).next().slideToggle(500);
      $(this).toggleClass('active');
    });

    return this;

  };

}(jQuery));

$(".acc").accordion();