#### Deliverables Format

- File should be a _.zip_ and named following next pattern: `WU-013__HW-{NAME_OF_HW}__{SURNAME_NAME}`;
  - example: `WU-013__HW-HTML_TAGS__BOND_JAMES`;
- Zip unarchiving should result in a folder with the same name as described above;
- Source files should be working, properly formatted and validated;
- __IMPORTANT!__ Remove all debuggers, console.log statements in functions!
- __IMPORTANT!__ Only declarations should be presented in file and not executions of those!
- __IMPORTANT!__ Do not remove code at the end of `script.js` file:
  ```
  module.exports = {
    getNumbers: getNumbers,
    getNumericSum: getNumericSum,
    getQuanOfUniqueElements: getQuanOfUniqueElements,
    getQuanOfUniqueCharacters: getQuanOfUniqueCharacters,
    getPensionersNames: getPensionersNames,
    groupByAge: groupByAge
  }
  ```

----

#### Task

- __getNumbers__ - a function that takes array of strings and returns array consisting of only valid numbers.  
_Invocation example_: `getNumbers(['2', '3', '4', 'cat', 'NaN']); // => [2, 3, 4]`  

- __getNumericSum__ - a function that takes aray of strings and returns a compound value of its numeric values.  
_Invocation example_: `getNumericSum(['2', '3', '4', 'cat', 'NaN']); // => 9`  
_Note_: Reuse __getNumbers__ function  

- __getQuanOfUniqueElements__ - a function that takes an array of elements and returns quantity of unique elements.  
_Invocation example_: `getQuanOfUniqueElements([3, 2, '2', 3]); // => 3`  
_Note_: consider using `indexOf`  

- __getQuanOfUniqueCharacters__ - a function which takes string and returns quantity of unique characters.  
_Invocation example_: `getQuanOfUniqueCharacters('muahaha'); // => 4`

- __getPensionersNames__ - a function that takes array of objects (think of human) in a following format: `{ name: 'John', age: 21 }` and returns array of names of people who are older than _65_.  
_Invocation example_: `getPensionersNames([ {name: 'Mary', age: 13}, {name: 'Nick', age: 65} ]); // => ['Nick']`  

- __groupByAge__ - a function that takes array of objects (same as in _getPensionersNames_) and returns an object with ages being keys and array of names being values.  
_Invocation example_:
```
var people = [
  {name: 'Mary', age: 13},
  {name: 'Nick', age: 65},
  {name: 'Mike', age: 13},
  {name: 'Kelly', age: 25}
];

groupByAge(people); // => { 13: ['Mary', 'Mike'], 25: ['Kelly'], 65: ['Nick']}
```
