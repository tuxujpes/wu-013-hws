/*******/
function getNumbers(a) {
  arr = [];
  for (var i = 0; i < a.length; i++) {
    if (!isNaN(+a[i])) arr.push(+a[i]);
  }
  return arr;
}
/*********/
function getNumericSum(data) {
  var result = 0;
  var numbers = getNumbers(data);
  for (var i = 0; i < numbers.length; i++) {
    result += numbers[i];
  }
  return result;
}
/*******/
function getQuanOfUniqueElements(arr) {
  var uniq = [];
  for (var i = 0; i < arr.length; i++) {
    if (uniq.indexOf(arr[i]) === -1) uniq.push(arr[i]);
  }
  return uniq.length;
}
/********/
function getQuanOfUniqueCharacters(str) {
  return getQuanOfUniqueElements(str);
}
/********/
function getPensionersNames(arr) {
  var pensionersNames = [];
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].age >= 65) pensionersNames.push(arr[i].name);
  }
  return pensionersNames;
}
/*******/
function groupByAge(arr) {
  var byAge = {};
  for (var i = 0; i < arr.length; i++) {
    var age = arr[i].age;
    if (!(age in byAge)) byAge[age] = [];
    byAge[age].push(arr[i].name);
  }
  return byAge;
}
/* DO NOT REMOVE EXPORTS BELOW */
module.exports = {
  getNumbers: getNumbers,
  getNumericSum: getNumericSum,
  getQuanOfUniqueElements: getQuanOfUniqueElements,
  getQuanOfUniqueCharacters: getQuanOfUniqueCharacters,
  getPensionersNames: getPensionersNames,
  groupByAge: groupByAge
}